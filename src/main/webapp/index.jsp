<%-- 
    Document   : index
    Created on : 29-04-2021, 17:17:18
    Author     : delaefe
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Entrega unidad 3 <br>Diego de la Fuente Curaqueo </h1>
         
        <small> base de datos de instrumentos musicales </small>
        
        <h2>GET: </h2>
        https://evaluacion3.herokuapp.com/api/instrumentos
        <br>
        
        <h2>POST:  </h2>
        https://evaluacion3.herokuapp.com/api/instrumentos
        <br>
        
        <h2>DELETE: </h2>
        https://evaluacion3.herokuapp.com/api/instrumentos/{ID}
        <br>
        
        <h2>PUT:  </h2> 
        https://evaluacion3.herokuapp.com/api/instrumentos
        <br>
        
        <br>
        <img src="https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Ffarm3.staticflickr.com%2F2021%2F2257604422_835692d397_z.jpg%3Fzz%3D1&f=1&nofb=1"></img>
        
    </body>
</html>
