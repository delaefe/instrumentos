/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restapi;

import dao.InstrumentosJpaController;
import dao.exceptions.NonexistentEntityException;
import entity.Instrumentos;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author delaefe
 */

@Path("instrumentos")
public class ApiInstrumentos {
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarSolicitudes(){
        
        //crea un dao para gestionar BD
        InstrumentosJpaController dao=new InstrumentosJpaController();
   
        //solicita lista con datos
        List<Instrumentos> lista= dao.findInstrumentosEntities();
   
        //envia respuesta
        return Response.ok(200).entity(lista).build();
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON) 
    public Response add(Instrumentos inst){
        
        //crea un dao para gestionar BD
        InstrumentosJpaController dao=new InstrumentosJpaController();
        
        //creamos elemento y lo ingresamos en bd
        try {
            dao.create(inst);
        } catch (Exception ex) {
            Logger.getLogger(ApiInstrumentos.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //envia respuesta  
        return Response.ok(200).entity(inst).build();
    }
    
    @DELETE
    @Path("/{iddelete}")
    @Produces(MediaType.APPLICATION_JSON) 
    public Response delete(@PathParam("iddelete") String iddelete){
        //crea un dao para gestionar BD
        InstrumentosJpaController dao=new InstrumentosJpaController();
            
        try {
            dao.destroy(iddelete);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(ApiInstrumentos.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.ok("cliente eliminado").build();
    }
    
    @PUT
    public Response update(Instrumentos inst){
        //crea un dao para gestionar BD
        InstrumentosJpaController dao=new InstrumentosJpaController();
        
        try {
            //modifica el elemento
            dao.edit(inst);
        } catch (Exception ex) {
            Logger.getLogger(ApiInstrumentos.class.getName()).log(Level.SEVERE, null, ex);
        }
 
        return Response.ok(200).entity(inst).build();
    }
}

