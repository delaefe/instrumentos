/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author delaefe
 */
@Entity
@Table(name = "instrumentos")
@NamedQueries({
    @NamedQuery(name = "Instrumentos.findAll", query = "SELECT i FROM Instrumentos i"),
    @NamedQuery(name = "Instrumentos.findByIdinstrumento", query = "SELECT i FROM Instrumentos i WHERE i.idinstrumento = :idinstrumento"),
    @NamedQuery(name = "Instrumentos.findByNombre", query = "SELECT i FROM Instrumentos i WHERE i.nombre = :nombre"),
    @NamedQuery(name = "Instrumentos.findByFamilia", query = "SELECT i FROM Instrumentos i WHERE i.familia = :familia"),
    @NamedQuery(name = "Instrumentos.findByRegistro", query = "SELECT i FROM Instrumentos i WHERE i.registro = :registro"),
    @NamedQuery(name = "Instrumentos.findByLlave", query = "SELECT i FROM Instrumentos i WHERE i.llave = :llave")})
public class Instrumentos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "idinstrumento")
    private String idinstrumento;
    @Size(max = 2147483647)
    @Column(name = "nombre")
    private String nombre;
    @Size(max = 2147483647)
    @Column(name = "familia")
    private String familia;
    @Size(max = 2147483647)
    @Column(name = "registro")
    private String registro;
    @Size(max = 2147483647)
    @Column(name = "llave")
    private String llave;

    public Instrumentos() {
    }

    public Instrumentos(String idinstrumento) {
        this.idinstrumento = idinstrumento;
    }

    public String getIdinstrumento() {
        return idinstrumento;
    }

    public void setIdinstrumento(String idinstrumento) {
        this.idinstrumento = idinstrumento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFamilia() {
        return familia;
    }

    public void setFamilia(String familia) {
        this.familia = familia;
    }

    public String getRegistro() {
        return registro;
    }

    public void setRegistro(String registro) {
        this.registro = registro;
    }

    public String getLlave() {
        return llave;
    }

    public void setLlave(String llave) {
        this.llave = llave;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idinstrumento != null ? idinstrumento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Instrumentos)) {
            return false;
        }
        Instrumentos other = (Instrumentos) object;
        if ((this.idinstrumento == null && other.idinstrumento != null) || (this.idinstrumento != null && !this.idinstrumento.equals(other.idinstrumento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Instrumentos[ idinstrumento=" + idinstrumento + " ]";
    }
    
}
