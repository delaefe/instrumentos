/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.exceptions.NonexistentEntityException;
import dao.exceptions.PreexistingEntityException;
import entity.Instrumentos;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author delaefe
 */
public class InstrumentosJpaController implements Serializable {

    public InstrumentosJpaController() {
     }
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistance_unit");

   
    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Instrumentos instrumentos) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(instrumentos);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findInstrumentos(instrumentos.getIdinstrumento()) != null) {
                throw new PreexistingEntityException("Instrumentos " + instrumentos + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Instrumentos instrumentos) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            instrumentos = em.merge(instrumentos);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = instrumentos.getIdinstrumento();
                if (findInstrumentos(id) == null) {
                    throw new NonexistentEntityException("The instrumentos with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Instrumentos instrumentos;
            try {
                instrumentos = em.getReference(Instrumentos.class, id);
                instrumentos.getIdinstrumento();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The instrumentos with id " + id + " no longer exists.", enfe);
            }
            em.remove(instrumentos);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Instrumentos> findInstrumentosEntities() {
        return findInstrumentosEntities(true, -1, -1);
    }

    public List<Instrumentos> findInstrumentosEntities(int maxResults, int firstResult) {
        return findInstrumentosEntities(false, maxResults, firstResult);
    }

    private List<Instrumentos> findInstrumentosEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Instrumentos.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Instrumentos findInstrumentos(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Instrumentos.class, id);
        } finally {
            em.close();
        }
    }

    public int getInstrumentosCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Instrumentos> rt = cq.from(Instrumentos.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
