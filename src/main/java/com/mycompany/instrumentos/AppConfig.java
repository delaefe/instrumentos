package com.mycompany.instrumentos;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Configures JAX-RS for the application.
 * @author Juneau
 */

//se cambia el nombre a api
@ApplicationPath("api")
public class AppConfig extends Application {
    
}
